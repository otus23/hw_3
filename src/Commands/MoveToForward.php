<?php

namespace Romqa\Commands;

use Romqa\Interfaces\Fuelable;
use Romqa\Interfaces\Movable;

class MoveToForward implements Command
{
    public function __construct(protected Fuelable $fuelable,
                                protected Movable  $movable)
    {
    }

    public function execute(): void
    {
        (new MacroCommand([
            new CheckFuelCommand($this->fuelable),
            new BurnFuelCommand($this->fuelable),
            new Move($this->movable)
        ]))->execute();
    }
}