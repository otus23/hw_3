<?php

namespace Romqa\Commands;

class MacroCommand implements Command
{
    /* @var Command[] $commands */
    protected array $commands;

    /**
     * @param Command[] $commands
     */
    public function __construct(array $commands)
    {
        $this->commands = $commands;
    }

    public function execute(): void
    {
        foreach ($this->commands as $command) {
            $command->execute();
        }
    }
}