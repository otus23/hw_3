<?php

namespace Romqa\Commands;

use Romqa\Interfaces\Directable;
use Romqa\Interfaces\Movable;
use Romqa\Math\Point;

class ChangeVelocityCommand implements Command
{
    public function __construct(protected Directable $rotatable,
                                protected Movable    $movable)
    {
    }

    public function execute(): void
    {
        $this->movable->setVelocity(Point::multiply($this->movable->getVelocity(), $this->rotatable->getDirection()));
    }
}