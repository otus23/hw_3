<?php

namespace Romqa\Commands;

use Romqa\Interfaces\Movable;
use Romqa\Interfaces\Rotatable;

class RotateMoved implements Command
{
    public function __construct(protected Rotatable $rotatable,
                                protected Movable   $movable)
    {
    }

    public function execute(): void
    {
        $this->rotatable->setDirection($this->rotatable->getDirection() + $this->rotatable->getStepDirection());
    }
}