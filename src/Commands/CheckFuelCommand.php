<?php

namespace Romqa\Commands;

use Romqa\Exception\CommandException;
use Romqa\Interfaces\Fuelable;

class CheckFuelCommand implements Command
{
    protected Fuelable $fuelable;

    public function __construct(Fuelable $fuelable)
    {
        $this->fuelable = $fuelable;
    }

    /**
     * @throws CommandException
     */
    public function execute(): void
    {
        if ($this->fuelable->getFuelLevel() < $this->fuelable->getFuelCost()) {
            throw new CommandException("low_fuel_level");
        }
    }
}