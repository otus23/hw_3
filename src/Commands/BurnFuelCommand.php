<?php

namespace Romqa\Commands;

use Romqa\Interfaces\Fuelable;

class BurnFuelCommand implements Command
{
    protected Fuelable $fuelable;

    public function __construct(Fuelable $fuelable)
    {
        $this->fuelable = $fuelable;
    }

    public function execute(): void
    {
        $this->fuelable->setFuelLevel($this->fuelable->getFuelLevel() - $this->fuelable->getFuelCost());
    }
}