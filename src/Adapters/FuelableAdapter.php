<?php

namespace Romqa\Adapters;

use Romqa\Interfaces\Fuelable;
use Romqa\Interfaces\UObject;

class FuelableAdapter implements Fuelable
{
    protected UObject $obj;

    public function __construct(UObject $obj)
    {
        $this->obj = $obj;
    }

    public function getFuelLevel(): int
    {
        return $this->obj->getProperty('fuel_level');
    }

    public function setFuelLevel(int $fuel_level): void
    {
        $this->obj->setProperty('fuel_level', $fuel_level);
    }

    public function getFuelCost(): int
    {
        return $this->obj->getProperty('fuel_cost');
    }

    public function setFuelCost(int $fuel_cost): void
    {
        $this->obj->setProperty('fuel_cost', $fuel_cost);
    }
}