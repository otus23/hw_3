<?php

namespace Romqa\Adapters;

use Romqa\Interfaces\Directable;
use Romqa\Interfaces\Movable;
use Romqa\Interfaces\UObject;
use Romqa\Math\Point;

class DirectionAdapter implements Directable
{
    protected UObject $obj;

    public function __construct(UObject $obj)
    {
        $this->obj = $obj;
    }

    public function getDirection(): Point
    {
        return $this->obj->getProperty('direction');
    }

    public function setDirection(Point $point): void
    {
        $this->obj->setProperty('direction', $point);
    }
}