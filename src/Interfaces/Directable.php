<?php

namespace Romqa\Interfaces;

use Romqa\Math\Point;

interface Directable
{
    public function getDirection(): Point;

    public function setDirection(Point $point): void;
}