<?php

namespace Romqa\Interfaces;

use Romqa\Math\Point;

interface Fuelable
{
    public function getFuelLevel(): int;

    public function setFuelLevel(int $fuel_level): void;

    public function getFuelCost(): int;

    public function setFuelCost(int $fuel_cost): void;
}