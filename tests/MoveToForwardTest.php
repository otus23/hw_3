<?php

use PHPUnit\Framework\TestCase;
use Romqa\Adapters\FuelableAdapter;
use Romqa\Adapters\MovableAdapter;
use Romqa\Commands\MoveToForward;
use Romqa\Exception\CommandException;
use Romqa\Math\Point;
use Romqa\Models\Tank;

class MoveToForwardTest extends TestCase
{
    public function testMoveToForward()
    {
        $tank = new Tank();

        $fuel_tank = new FuelableAdapter($tank);
        $fuel_tank->setFuelLevel(30);
        $fuel_tank->setFuelCost(10);
        $movable_tank = new MovableAdapter($tank);
        $movable_tank->setPosition(new Point(0, 0));
        $movable_tank->setVelocity(new Point(10, 10));

        $command = new MoveToForward($fuel_tank, $movable_tank);
        $command->execute();

        $this->assertEquals($fuel_tank->getFuelLevel(), 20);
    }

    public function testMoveToForwardFail()
    {
        $tank = new Tank();

        $this->expectException(CommandException::class);
        $fuel_tank = new FuelableAdapter($tank);
        $fuel_tank->setFuelLevel(30);
        $fuel_tank->setFuelCost(40);
        $movable_tank = new MovableAdapter($tank);
        $movable_tank->setPosition(new Point(0, 0));
        $movable_tank->setVelocity(new Point(10, 10));

        $command = new MoveToForward($fuel_tank, $movable_tank);
        $command->execute();

        $this->assertEquals($fuel_tank->getFuelLevel(), 20);
    }
}