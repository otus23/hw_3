<?php

use PHPUnit\Framework\TestCase;
use Romqa\Adapters\FuelableAdapter;
use Romqa\Commands\BurnFuelCommand;
use Romqa\Commands\CheckFuelCommand;
use Romqa\Exception\CommandException;
use Romqa\Models\Tank;

class CheckFuelTest extends TestCase
{
    public function testCheckFuelSuccess()
    {
        $tank = new Tank();
        $tank->setProperty('fuel_level', 30);
        $tank->setProperty('fuel_cost', 10);

        $fuel_tank = new FuelableAdapter($tank);
        $fuel_burn = new CheckFuelCommand($fuel_tank);
        $fuel_burn->execute();

        $this->assertEquals(true,true);
    }

    public function testCheckFuelFail()
    {
        $this->expectException(CommandException::class);
        $tank = new Tank();
        $tank->setProperty('fuel_level', 30);
        $tank->setProperty('fuel_cost', 50);

        $fuel_tank = new FuelableAdapter($tank);
        $fuel_burn = new CheckFuelCommand($fuel_tank);
        $fuel_burn->execute();
    }
}