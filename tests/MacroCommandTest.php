<?php

use PHPUnit\Framework\TestCase;
use Romqa\Commands\Command;
use Romqa\Commands\MacroCommand;
use Romqa\Exception\CommandException;

class MacroCommandTest extends TestCase
{
    public function testMacroCommandFail()
    {
        $mock = $this->createMock(Command::class);
        $mock->method('execute')->willThrowException(new CommandException(''));
        $this->expectException(CommandException::class);

        $command = new MacroCommand([
            $mock,
            $mock
        ]);
        $command->execute();
    }
}