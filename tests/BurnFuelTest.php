<?php

use PHPUnit\Framework\TestCase;
use Romqa\Adapters\FuelableAdapter;
use Romqa\Commands\BurnFuelCommand;
use Romqa\Models\Tank;

class BurnFuelTest extends TestCase
{
    public function testBurningFuel()
    {
        $tank = new Tank();
        $tank->setProperty('fuel_level', 30);
        $tank->setProperty('fuel_cost', 10);

        $fuel_tank = new FuelableAdapter($tank);
        $fuel_burn = new BurnFuelCommand($fuel_tank);
        $fuel_burn->execute();

        $this->assertEquals($fuel_tank->getFuelLevel(), 20);
    }
}